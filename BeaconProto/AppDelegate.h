//
//  AppDelegate.h
//  BeaconProto
//
//  Created by Tod Dooshik Chung on 5/2/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BeaconScanner.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>{
    BeaconScanner*beaconScanner;
    CLLocationManager * locationManager;
    
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,retain) BeaconScanner*beaconScanner;
@property (nonatomic,retain) CLLocationManager* locationManager;

@end
