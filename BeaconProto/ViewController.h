//
//  ViewController.h
//  BeaconProto
//
//  Created by Tod Dooshik Chung on 5/2/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "BeaconScanner.h"
#import "AddBeaconViewController.h"

@interface ViewController : UIViewController<KnownBeaconScannerDelegate,UITableViewDataSource,UITableViewDelegate>{
    IBOutlet UITableView*tv;    
    NSMutableArray*knownBeacons;
}


- (void)didUpdateKnownBeaconList:(NSMutableDictionary *)beaconAndInfosDic;
- (void)addBeacon:(CLBeacon*)aBeacon withMessage:(NSString*)message;

@end