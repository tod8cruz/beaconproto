//
//  BeaconScanner.m
//  BeaconProto
//
//  Created by Tod Dooshik Chung on 5/2/14.
//  Copyright (c) 2014 Tod Dooshik Chung. All rights reserved.
//

#import "BeaconScanner.h"

@implementation BeaconScanner

@synthesize knownBeaconDelegate;
@synthesize unknownBeaconDelegate;


-(id)init{
    self = [super init];
    if (self) {
        knownBeacons=[[NSMutableArray alloc]init];
        unknownBeacons=[[NSMutableArray alloc]init];
        sqlManager=[[SQLManager alloc]init];
        
        [sqlManager scanAndLogTable];
    }
    return self;
}

-(void)startScan{
    // Check if beacon monitoring is available for this device
    if (![CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Monitoring not available" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]; [alert show]; return;
        [knownBeaconDelegate didUpdateKnownBeaconList:nil];
        [unknownBeaconDelegate didUpdateUnknownBeaconList:nil];
    }
    //ToDo: authorizationStatus가 kCLAuthorizationStatusAuthorized인지 확인하는 코드 필요
    
    mLocationManager = [[CLLocationManager alloc] init];
    mLocationManager.delegate = self;
    
    
//    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"];
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"2F234454-CF6D-4A0F-ADF2-F4911BA9FFA6"];
    mBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:@"com.8cruz.Estimote"];
    mBeaconRegion.notifyEntryStateOnDisplay=YES;
    mBeaconRegion.notifyOnEntry=YES;
    mBeaconRegion.notifyOnExit=YES;
    
    [mLocationManager startMonitoringForRegion:mBeaconRegion];
    [mLocationManager requestStateForRegion:mBeaconRegion];
}


-(void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region{
    NSLog(@"%s",__FUNCTION__);
    if(state==CLRegionStateInside && [CLLocationManager isRangingAvailable]){
        [mLocationManager startRangingBeaconsInRegion:mBeaconRegion];
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
//    NSLog(@"%s",__FUNCTION__);
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"Beacon Found");
    [mLocationManager startRangingBeaconsInRegion:mBeaconRegion];
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSLog(@"Left Region");
    [mLocationManager stopRangingBeaconsInRegion:mBeaconRegion];
    //ToDo: 새로운 Delegate method를 만들어서 ViewController로 알려주는 기능&scannedBeacon에서 제외하는 기능 추가필요
}

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
//ToDo: 여러종류의 UUID가 들어오는 경우의 처리 필요. 지금은 하나의 UUID가정.
    
    [knownBeacons removeAllObjects];
    [unknownBeacons removeAllObjects];
    
    for(CLBeacon *aBeacon in beacons){//addToScanned Beacons
        NSMutableDictionary*beaconInfoDic=[sqlManager getBeaconInfoForUUID:aBeacon.proximityUUID.UUIDString
                                                          major:[aBeacon.major intValue]
                                                          minor:[aBeacon.minor intValue]
                                   ];
        NSString*title=[beaconInfoDic objectForKey:@"title"];
        NSString*message=[beaconInfoDic objectForKey:@"message"];
        
        
        if(beaconInfoDic!=NULL && (aBeacon.proximity==CLProximityNear || aBeacon.proximity==CLProximityImmediate)){//add to knownBeaconList
            NSMutableDictionary*aBeaconDic=[[NSMutableDictionary alloc]init];
            [aBeaconDic setObject:aBeacon forKey:@"beacon"];
            [aBeaconDic setObject:title forKey:@"title"];
            [aBeaconDic setObject:message forKey:@"message"];
            
            [knownBeacons addObject:aBeaconDic];
        }
        else if(aBeacon.proximity==CLProximityNear || aBeacon.proximity==CLProximityImmediate){
            [unknownBeacons addObject:aBeacon];
        }
    }
    
    
    [knownBeaconDelegate didUpdateKnownBeaconList:knownBeacons];
    [unknownBeaconDelegate didUpdateUnknownBeaconList:unknownBeacons];
}
@end

//    majorLabel.text = [NSString stringWithFormat:@"%@", beacon.major];
//    minorLabel.text = [NSString stringWithFormat:@"%@", beacon.minor];
//    accuracyLabel.text = [NSString stringWithFormat:@"%f", beacon.accuracy];
//    if (beacon.proximity == CLProximityUnknown) {
//        distanceLabel.text = @"Unknown Proximity";
//    } else if (beacon.proximity == CLProximityImmediate) {
//        distanceLabel.text = @"Immediate";
//    } else if (beacon.proximity == CLProximityNear) {
//        distanceLabel.text = @"Near";
//    } else if (beacon.proximity == CLProximityFar) {
//        distanceLabel.text = @"Far";
//    }
//    rssiLabel.text = [NSString stringWithFormat:@"%i", beacon.rssi];