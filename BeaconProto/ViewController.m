//
//  ViewController.m
//  BeaconProto
//
//  Created by Tod Dooshik Chung on 5/2/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "ViewController.h"
#import "AddBeaconViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    BeaconScanner*beaconScanner=((AppDelegate*)[[UIApplication sharedApplication] delegate]).beaconScanner;
    beaconScanner.knownBeaconDelegate=self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AddBeacon"]) {
//        UINavigationController *nav = [segue destinationViewController];
//        AddBeaconViewController *destViewController = (AddBeaconViewController *)nav.topViewController;
//        destViewController.delegate=self;
//        destViewController.unknownBeacons = unknownBeacons;
    }
}


#pragma mark -
#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return MAX([knownBeacons count],1);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if([knownBeacons count]==0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"NoBeacon"];
    }
    else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"KnownBeacons"];
        
        NSMutableDictionary*beaconDic=(NSMutableDictionary*)[knownBeacons objectAtIndex:[indexPath row]];

        CLBeacon*aBeacon=[beaconDic objectForKey:@"beacon"];
        
        UILabel *title = (UILabel *)[cell viewWithTag:1];
        title.text=[beaconDic objectForKey:@"title"];
        

        
        UILabel *beaconDetail = (UILabel *)[cell viewWithTag:2];
        beaconDetail.text=[NSString stringWithFormat:@"Major: %d | Minor: %d | 거리: %.02fm",
                           [aBeacon.major intValue],
                           [aBeacon.minor intValue],
                           [self calculateAccuracyForRSSI:aBeacon.rssi]
                           ];
        
        UITextField*textField=(UITextField*)[cell viewWithTag:3];
        textField.text=[beaconDic objectForKey:@"message"];
    }
    
    return cell;
}

#pragma mark -
#pragma mark BeaconScanner delegate
- (void)didUpdateKnownBeaconList:(NSMutableArray *)beacons{
    knownBeacons=beacons;
    [tv reloadData];
}

#pragma mark -
#pragma mark AddBeacon delegate
- (void)addBeacon:(CLBeacon*)aBeacon withMessage:(NSString*)message{
//    NSLog(@"Message: %@",message);
//    [self dismissViewControllerAnimated:YES completion:nil];
//    
//    BOOL success=[sqlManager setMessage:message
//                   ForUUID:aBeacon.proximityUUID.UUIDString
//                     major:[aBeacon.major intValue]
//                     minor:[aBeacon.minor intValue]
//     ];
//    if(success){
//        [unknownBeacons removeObject:aBeacon];
//        [knownBeacons addObject:aBeacon];
//        [messagesForKnownBeacons addObject:message];
//        [tv reloadData];
//    }
}

- (double) calculateAccuracyForRSSI:(double) rssi{
    int txPower=-74;
    if (rssi == 0) {
        return -1.0; // if we cannot determine accuracy, return -1.
    }
    
    double ratio = rssi*1.0/txPower;
    if (ratio < 1.0) {
        return pow(ratio,10);
    }
    else {
        double accuracy =  (0.89976)*pow(ratio,7.7095) + 0.111;
        return accuracy;
    }
}
@end