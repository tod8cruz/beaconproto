//
//  SQLManager.m
//  BeaconProto
//
//  Created by Tod Dooshik Chung on 5/6/14.
//  Copyright (c) 2014 Tod Dooshik Chung. All rights reserved.
//

#import "SQLManager.h"

@implementation SQLManager

- (id)init {
    self = [super init];
    if (self) {
        
        //### Copy bundle DB to local document(Because bundle db is readonly)
        NSString *bundleDBPath = [[[NSBundle mainBundle] resourcePath ]stringByAppendingPathComponent:@"BeaconsDB.sqlite"];
        NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [pathsToDocuments objectAtIndex:0];
        NSString *copiedDBPath = [documentsDirectory stringByAppendingPathComponent:@"BeaconsDB.sqlite"];
        
        if (![[NSFileManager defaultManager] isReadableFileAtPath:copiedDBPath]) {
            if ([[NSFileManager defaultManager] copyItemAtPath:bundleDBPath toPath:copiedDBPath error:NULL] != YES){
                NSLog(@"copy failed");
            }
        }
        //___
        
        ////////
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        BOOL success = [fileMgr fileExistsAtPath:copiedDBPath];
        if(!success)
        {
            NSLog(@"Cannot locate database file '%@'.", copiedDBPath);
        }
        if(!(sqlite3_open([copiedDBPath UTF8String], &db) == SQLITE_OK))
        {
            NSLog(@"An error has occured.");
        }
    }
    return self;
}

-(void)scanAndLogTable{
    @try {
        NSString*sql=[NSString stringWithFormat:@"Select Title,Message,Major,Minor FROM Beacons"];
        sqlite3_stmt *sqlStatement;
        sqlite3_prepare(db, [sql UTF8String], -1, &sqlStatement, NULL);
        while (sqlite3_step(sqlStatement)==SQLITE_ROW) {
            char*titleChar=(char*)sqlite3_column_text(sqlStatement, 0);
            char*messageChar=(char*)sqlite3_column_text(sqlStatement, 1);
            NSLog(@"Title: %@",[NSString stringWithUTF8String:titleChar]);
            NSLog(@"Message: %@",[NSString stringWithUTF8String:messageChar]);
            NSLog(@"Major: %d",sqlite3_column_int(sqlStatement, 2));
            NSLog(@"Minor: %d",sqlite3_column_int(sqlStatement, 3));
            NSLog(@"############");
        }

    }
    @catch (NSException *exception) {
        NSLog(@"%s\nAn exception occured: %@",__FUNCTION__, [exception reason]);
    }
}

-(NSMutableDictionary*)getBeaconInfoForUUID:(NSString*)uuid major:(int)major minor:(int)minor{
    NSMutableDictionary*ret=[[NSMutableDictionary alloc]init];
    @try {
        NSString*sql=[NSString stringWithFormat:@"Select Title,Message FROM Beacons WHERE UUID='%@' AND Major=%d AND Minor=%d",uuid,major,minor];
//        NSLog(@"%s: %@",__FUNCTION__,sql);
        sqlite3_stmt *sqlStatement;
        if(sqlite3_prepare(db, [sql UTF8String], -1, &sqlStatement, NULL) != SQLITE_OK){
            NSLog(@"%s SQL query no result",__FUNCTION__);
            return NULL;
        }
        BOOL didFindInfo=NO;
        while (sqlite3_step(sqlStatement)==SQLITE_ROW) {
            didFindInfo=YES;
            char*titleChar=(char*)sqlite3_column_text(sqlStatement, 0);
            char*messageChar=(char*)sqlite3_column_text(sqlStatement, 1);
            
            [ret setObject:[NSString stringWithUTF8String:titleChar] forKey:@"title"];
            [ret setObject:[NSString stringWithUTF8String:messageChar] forKey:@"message"];
        }
        if(didFindInfo)return ret;
        return NULL;
        
    }
    @catch (NSException *exception) {
        NSLog(@"%s\nAn exception occured: %@",__FUNCTION__, [exception reason]);
        return NULL;
    }
    
    
}
-(BOOL)setTitle:(NSString*)title andMessage:(NSString*)message ForUUID:(NSString*)uuid major:(int)major minor:(int)minor{
    @try {
        NSString*sql = [NSString stringWithFormat:@"INSERT INTO Beacons ('UUID','Major','Minor','Title','Message') VALUES ('%@',%d,%d,'%@','%@')",uuid,major,minor,title,message];
        NSLog(@"%s\nSQL: %@",__FUNCTION__,sql);
        
        sqlite3_stmt *sqlStatement;
        sqlite3_prepare_v2(db, [sql UTF8String], -1, &sqlStatement, NULL);
        int sqlStep=sqlite3_step(sqlStatement);
        if(sqlStep == SQLITE_DONE) {
            NSLog(@"%s inserting '%@' success",__FUNCTION__,message);
        }
        else{
            NSLog(@"%s inserting '%@' fail",__FUNCTION__,message);
            NSLog(@"sql step=%d",sqlStep);
            return NO;
        }
        
        sqlite3_finalize(sqlStatement);
    }
    @catch (NSException *exception) {
        NSLog(@"%s\nAn exception occured: %@",__FUNCTION__, [exception reason]);
        return NO;
    }
    return YES;
}

- (void) dealloc
{
    sqlite3_close(db);
}

@end
