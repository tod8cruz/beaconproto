//
//  AddBeaconDetailEdit.h
//  BeaconProto
//
//  Created by Tod Dooshik Chung on 5/7/14.
//  Copyright (c) 2014 Tod Dooshik Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AddBeaconDetailEdit : UIViewController{
    CLBeacon*beacon;
    IBOutlet UILabel*beaconInfo;
    IBOutlet UITextField*titleTextField;
    IBOutlet UITextView*messageTextView;
}
@property(nonatomic,retain) CLBeacon*beacon;
- (IBAction)didTouchDone:(id)sender;

@end
