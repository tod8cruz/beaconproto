//
//  AddBeaconDetailEdit.m
//  BeaconProto
//
//  Created by Tod Dooshik Chung on 5/7/14.
//  Copyright (c) 2014 Tod Dooshik Chung. All rights reserved.
//

#import "AddBeaconDetailEdit.h"
#import "SQLManager.h"

@interface AddBeaconDetailEdit ()

@end




@implementation AddBeaconDetailEdit

@synthesize beacon;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    double distance=[self calculateAccuracyForRSSI:(double)beacon.rssi];
    
    
    beaconInfo.text=[NSString stringWithFormat:@"Major: %d | Minor: %d | 거리: %.02fm",
                     [beacon.major intValue],
                     [beacon.minor intValue],
                      (double)distance
                     ];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTouchDone:(id)sender{
    if([titleTextField.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"제목을 입력하세요." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]; [alert show]; return;
    }
    if([messageTextView.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"설명을 입력하세요." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]; [alert show]; return;
    }
    SQLManager*sqlManager=[[SQLManager alloc]init];
    BOOL success=[sqlManager setTitle:titleTextField.text
              andMessage:messageTextView.text
                 ForUUID:beacon.proximityUUID.UUIDString
                   major:[beacon.major intValue]
                   minor:[beacon.minor intValue]
     ];
    
    if(success==YES){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"비콘정보 입력에 실패하였습니다." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]; [alert show]; return;
    }
}


- (double) calculateAccuracyForRSSI:(double) rssi{
    int txPower=-74;
    if (rssi == 0) {
        return -1.0; // if we cannot determine accuracy, return -1.
    }
    
    double ratio = rssi*1.0/txPower;
    if (ratio < 1.0) {
        return pow(ratio,10);
    }
    else {
        double accuracy =  (0.89976)*pow(ratio,7.7095) + 0.111;
        return accuracy;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
