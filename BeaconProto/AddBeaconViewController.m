//
//  AddBeaconViewController.m
//  BeaconProto
//
//  Created by Tod Dooshik Chung on 5/6/14.
//  Copyright (c) 2014 Tod Dooshik Chung. All rights reserved.
//

#import "AddBeaconViewController.h"
#import "BeaconScanner.h"
#import "AppDelegate.h"
#import "AddBeaconDetailEdit.h"

@interface AddBeaconViewController (){
    
}

@end

@implementation AddBeaconViewController

@synthesize mUnknownBeacons;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    BeaconScanner*beaconScanner=((AppDelegate*)[[UIApplication sharedApplication] delegate]).beaconScanner;
    beaconScanner.unknownBeaconDelegate=self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"beaconSelect"]) {
        NSIndexPath *indexPath = [mTv indexPathForSelectedRow];
        AddBeaconDetailEdit *destViewController = segue.destinationViewController;
        destViewController.beacon = [mUnknownBeacons objectAtIndex:[indexPath row]];
    }
}

#pragma mark -
#pragma mark BeaconScannerDelegate
- (void)didUpdateUnknownBeaconList:(NSMutableArray*)unknownBeacons{
    mUnknownBeacons=unknownBeacons;
    [mTv reloadData];
}



#pragma mark -
#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return MAX([mUnknownBeacons count],1);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if([mUnknownBeacons count]==0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"NoBeacon"];
    }
    else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"Beacons"];
        CLBeacon*aBeacon=[mUnknownBeacons objectAtIndex:[indexPath row]];
        
        UILabel *majorLabel = (UILabel *)[cell viewWithTag:1];
        majorLabel.text=[NSString stringWithFormat:@"%d",[aBeacon.major intValue]];
        
        UILabel *minorLabel = (UILabel *)[cell viewWithTag:2];
        minorLabel.text=[NSString stringWithFormat:@"%d",[aBeacon.minor intValue]];
        UILabel *distanceLabel = (UILabel *)[cell viewWithTag:3];
        distanceLabel.text=[NSString stringWithFormat:@"%.02fm",[self calculateAccuracyForRSSI:aBeacon.rssi]];
    }


    return cell;
}

- (double) calculateAccuracyForRSSI:(double) rssi{
    int txPower=-74;
    if (rssi == 0) {
        return -1.0; // if we cannot determine accuracy, return -1.
    }
    
    double ratio = rssi*1.0/txPower;
    if (ratio < 1.0) {
        return pow(ratio,10);
    }
    else {
        double accuracy =  (0.89976)*pow(ratio,7.7095) + 0.111;
        return accuracy;
    }
}




- (IBAction)didTouchCancel:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end