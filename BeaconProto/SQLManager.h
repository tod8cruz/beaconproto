//
//  SQLManager.h
//  BeaconProto
//
//  Created by Tod Dooshik Chung on 5/6/14.
//  Copyright (c) 2014 Tod Dooshik Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
@interface SQLManager : NSObject{
    sqlite3 *db;
}

-(void)scanAndLogTable;
-(NSMutableDictionary*)getBeaconInfoForUUID:(NSString*)uuid major:(int)major minor:(int)minor;
-(BOOL)setTitle:(NSString*)title andMessage:(NSString*)message ForUUID:(NSString*)uuid major:(int)major minor:(int)minor;
@end
