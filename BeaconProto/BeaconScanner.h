//
//  BeaconScanner.h
//  BeaconProto
//
//  Created by Tod Dooshik Chung on 5/2/14.
//  Copyright (c) 2014 Tod Dooshik Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "SQLManager.h"

@protocol KnownBeaconScannerDelegate
- (void)didUpdateKnownBeaconList:(NSMutableArray*)knownBeacons;
@end

@protocol UnknownBeaconScannerDelegate
- (void)didUpdateUnknownBeaconList:(NSMutableArray*)unknownBeacons;
@end

@interface BeaconScanner : NSObject <CLLocationManagerDelegate>{
    id<KnownBeaconScannerDelegate> knownBeaconDelegate;
    id<UnknownBeaconScannerDelegate> unknownBeaconDelegate;
    CLBeaconRegion *mBeaconRegion;
    CLLocationManager *mLocationManager;

    NSMutableArray *knownBeacons;
    NSMutableArray *unknownBeacons;
    SQLManager*sqlManager;
}

@property(nonatomic,retain) id<KnownBeaconScannerDelegate> knownBeaconDelegate;
@property(nonatomic,retain) id<UnknownBeaconScannerDelegate> unknownBeaconDelegate;

-(void)startScan;

@end