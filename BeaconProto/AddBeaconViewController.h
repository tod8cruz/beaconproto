//
//  AddBeaconViewController.h
//  BeaconProto
//
//  Created by Tod Dooshik Chung on 5/6/14.
//  Copyright (c) 2014 Tod Dooshik Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "BeaconScanner.h"


@interface AddBeaconViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UnknownBeaconScannerDelegate>{
    IBOutlet UITableView*mTv;
    NSMutableArray*mUnknownBeacons;
}


@property(nonatomic,retain) NSMutableArray*mUnknownBeacons;

- (IBAction)didTouchCancel:(id)sender;

@end
